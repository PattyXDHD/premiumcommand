package de.pattyxdhd.premium.commands;

import de.pattyxdhd.premium.PremiumMain;
import de.pattyxdhd.premium.utils.objects.PremiumPlayer;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Date;

public class PremiumCommand implements CommandExecutor {


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(!(sender instanceof Player)){
            sender.sendMessage("Du musst ein Spieler sein.");
            return false;
        }

        final Player player = ((Player) sender);

        if(sender.hasPermission("premium.use")){

            if (args.length == 1){

                final PremiumPlayer premiumPlayer = PremiumMain.getInstance().getPlayers().get(player.getUniqueId());

                if(premiumPlayer == null){
                    sender.sendMessage("§4Error: §cPremiumPlayer not found.");
                    return false;
                }

                final String targetName = args[0];

                if ((premiumPlayer.getLastExecute()+(PremiumMain.getInstance().getFileManager().getCoolDownSeconds()*1000)) <= System.currentTimeMillis()){

                    premiumPlayer.setLastExecute(System.currentTimeMillis());
                    Bukkit.dispatchCommand(Bukkit.getConsoleSender(), PremiumMain.getInstance().getFileManager().getPremiumSetCommand().replace("%playerName%", targetName).replace("%seconds%", PremiumMain.getInstance().getFileManager().getPremiumSeconds()+""));
                    sender.sendMessage(PremiumMain.getInstance().getFileManager().getPrefix() + "§aDu hast §e" + targetName + " §afür §e" + getTimeDifference(System.currentTimeMillis(), PremiumMain.getInstance().getFileManager().getPremiumSeconds()) + " Premium gegeben.");

                }else{
                    sender.sendMessage(PremiumMain.getInstance().getFileManager().getPrefix() + "§cDu musst noch bis §e" + PremiumMain.getInstance().getFileManager().getSimpleDateFormat().format(premiumPlayer.getLastExecute()+(PremiumMain.getInstance().getFileManager().getCoolDownSeconds()*1000)) + " §cwarten.");
                }

            }else {
                sender.sendMessage(PremiumMain.getInstance().getFileManager().getPrefix() + "Usage: /premium <Player>");
            }

        }else {
            sender.sendMessage(PremiumMain.getInstance().getFileManager().getPrefix() + "§cDazu hast du keinen Zugriff.");
        }


        return false;
    }

    public String getTimeDifference(final long start, final long end){

        if(end <= -1){
            return "§4Permanent";
        }

        Date startDate = new Date(start);
        Date endDate = new Date(System.currentTimeMillis() + (end*1000));

        long different = endDate.getTime() - startDate.getTime();

        long secondsInMilli = 1000;
        long minutesInMilli = secondsInMilli * 60;
        long hoursInMilli = minutesInMilli * 60;
        long daysInMilli = hoursInMilli * 24;

        long elapsedDays = different / daysInMilli;
        different = different % daysInMilli;

        long elapsedHours = different / hoursInMilli;
        different = different % hoursInMilli;

        long elapsedMinutes = different / minutesInMilli;
        different = different % minutesInMilli;

        long elapsedSeconds = different / secondsInMilli;

        if (elapsedMinutes != 0){
            return elapsedDays + " Tage, " + elapsedHours + " Stunden, " + elapsedMinutes + " Minuten";
        }

        if (elapsedHours != 0){
            return elapsedDays + " Tage, " + elapsedHours + " Stunden";
        }
        return elapsedDays + " Tage";
    }

}
