package de.pattyxdhd.premium.utils.file;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;

@Getter @Setter(AccessLevel.PRIVATE)
public class FileManager {

    private FileWriter fileWriter;

    private boolean useSQLite = true;
    private String host = "127.0.0.1";
    private int port = 3306;
    private String database = "PremiumCommand";
    private String user = "PremiumCommand";
    private String password = "SicheresPassword";

    private String prefix = "§8[§6Premium§8] §7";
    private String dateFormat = "dd.MM.yyyy HH:mm";

    private int coolDownSeconds = 604800;
    private int premiumSeconds = 604800;

    private String premiumSetCommand = "lp user %playerName% parent addtemp premium %seconds%s";


    public FileManager(FileWriter fileWriter) {
        this.fileWriter = fileWriter;
        load();
        read();
    }

    private void load(){

        fileWriter.setDefaultValue("MySQL.useSQLite", useSQLite);
        fileWriter.setDefaultValue("MySQL.Host", host);
        fileWriter.setDefaultValue("MySQL.Port", port);
        fileWriter.setDefaultValue("MySQL.Database", database);
        fileWriter.setDefaultValue("MySQL.User", user);
        fileWriter.setDefaultValue("MySQL.Password", password);

        fileWriter.setDefaultValue("Message.Prefix", prefix);
        fileWriter.setDefaultValue("Message.dateFormat", dateFormat);

        fileWriter.setDefaultValue("CoolDownSeconds", coolDownSeconds);
        fileWriter.setDefaultValue("PremiumSeconds", premiumSeconds);

        fileWriter.setDefaultValue("PremiumSetCommand", premiumSetCommand);

    }

    private void read(){

        setUseSQLite(fileWriter.getBoolean("MySQL.useSQLite"));
        setHost(fileWriter.getString("MySQL.Host"));
        setPort(fileWriter.getInt("MySQL.Port"));
        setDatabase(fileWriter.getString("MySQL.Database"));
        setUser(fileWriter.getString("MySQL.User"));
        setPassword(fileWriter.getString("MySQL.Password"));

        setPrefix(fileWriter.getFormatString("Message.Prefix"));
        setDateFormat(fileWriter.getString("Message.dateFormat"));

        setCoolDownSeconds(fileWriter.getInt("CoolDownSeconds"));
        setPremiumSeconds(fileWriter.getInt("PremiumSeconds"));

        setPremiumSetCommand(fileWriter.getString("PremiumSetCommand"));

    }

    public SimpleDateFormat getSimpleDateFormat(){
        return new SimpleDateFormat(getDateFormat());
    }

}
