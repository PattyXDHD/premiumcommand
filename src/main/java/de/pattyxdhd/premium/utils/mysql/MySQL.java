package de.pattyxdhd.premium.utils.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MySQL {

    private String host;
    private Integer port = 3306;
    private String database;
    private String user;
    private String password;
    private String prefix = "[MySQL-API] ";
    private boolean sqLite = true;

    private Connection connection;

    private final ExecutorService service = Executors.newCachedThreadPool();

    public MySQL(String host, Integer port, String database, String user, String password, String prefix, boolean sqLight) {
        this.host = host;
        this.port = port;
        this.database = database;
        this.user = user;
        this.password = password;
        if(prefix != null){
            this.prefix = prefix;
        }
        this.sqLite = sqLight;
    }

    public void connect(){
        if (isConnected()) return;

        String url;

        try {
            if(sqLite){
                url = "jdbc:sqlite://" + host;
                connection = DriverManager.getConnection(url);
            }else{
                url = "jdbc:mysql://" + host + ":" + port + "/" + database + "?autoReconnect=true&useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
                connection = DriverManager.getConnection(url, user, password);
            }
            System.out.println(prefix + "Datenbank wurde verbunden.");
        } catch (SQLException e) {
            connection = null;
            e.printStackTrace();
            System.out.println(prefix + "Datenbank konnte nicht verbunden werden.");
        }
    }

    public void disconnect(){
        if (!isConnected()) return;

        try {
            connection.close();
            connection = null;
            System.out.println(prefix + "Datenbank wurde getrennt.");
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println(prefix + "Datenbank konnte nicht getrennt werden.");
        }
    }

    public boolean isConnected(){
        return connection != null;
    }

    public Connection getConnection() {
        return connection;
    }

    public PreparedStatement preparedStatement(final String sql){
        try {
            return this.connection.prepareStatement(sql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void syncUpdate(final PreparedStatement preparedStatement) throws SQLException{
        preparedStatement.execute();
        preparedStatement.close();
    }

    public void update(final PreparedStatement preparedStatement){
        this.service.execute(() -> {
            try {

                syncUpdate(preparedStatement);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

}
