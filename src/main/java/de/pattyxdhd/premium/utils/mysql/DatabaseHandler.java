package de.pattyxdhd.premium.utils.mysql;

import de.pattyxdhd.premium.PremiumMain;
import de.pattyxdhd.premium.utils.objects.PremiumPlayer;
import lombok.Getter;

import java.sql.ResultSet;

public class DatabaseHandler {

    @Getter
    private MySQL mySQL;

    public DatabaseHandler(MySQL mySQL) {
        this.mySQL = mySQL;
    }

    public void loadPlayers(){
        try {
            PremiumMain.getInstance().getPlayers().clear();
            final ResultSet resultSet = getMySQL().preparedStatement("SELECT * FROM PremiumPlayers").executeQuery();

            while (resultSet.next()){
                final PremiumPlayer premiumPlayer = new PremiumPlayer(resultSet.getString("uuid"), resultSet.getLong("lastExecute"));
                PremiumMain.getInstance().getPlayers().put(premiumPlayer.getUniqueId(), premiumPlayer);
            }
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            PremiumMain.getInstance().setPlayersLoaded(true);
            PremiumMain.getInstance().log("§aPlayer cache geladen.");
        }
    }

    public PremiumPlayer getPlayer(final String uuid){
        PremiumPlayer premiumPlayer = null;

        try {
            final ResultSet resultSet = new PreparedStatementBuilder("SELECT * FROM PremiumPlayers WHERE uuid=?", getMySQL()).bindString(uuid).build().executeQuery();

            while (resultSet.next()){
                premiumPlayer = new PremiumPlayer(resultSet.getString("uuid"), resultSet.getLong("lastExecute"));
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return premiumPlayer;
    }

    public boolean existsPlayer(final String uuid){
        try {
            final ResultSet resultSet = new PreparedStatementBuilder("SELECT * FROM PremiumPlayers WHERE uuid=?", getMySQL())
                    .bindString(uuid)
                    .build().executeQuery();

            return resultSet.next();
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    public void insertPlayer(final PremiumPlayer premiumPlayer){
        getMySQL().update(new PreparedStatementBuilder("INSERT INTO PremiumPlayers(uuid, lastExecute) VALUES (?, ?)", getMySQL())
                .bindString(premiumPlayer.getUuid())
                .bindLong(premiumPlayer.getLastExecute())
                .build());

        PremiumMain.getInstance().getPlayers().put(premiumPlayer.getUniqueId(), premiumPlayer);
    }

    public void updatePlayer(final PremiumPlayer premiumPlayer){
        getMySQL().update(new PreparedStatementBuilder("UPDATE PremiumPlayers SET lastExecute=? WHERE uuid=?", getMySQL())
                .bindLong(premiumPlayer.getLastExecute())
                .bindString(premiumPlayer.getUuid())
                .build());

        if(PremiumMain.getInstance().getPlayers().containsKey(premiumPlayer.getUniqueId())) PremiumMain.getInstance().getPlayers().remove(premiumPlayer.getUniqueId());
        PremiumMain.getInstance().getPlayers().put(premiumPlayer.getUniqueId(), premiumPlayer);
    }

}
