package de.pattyxdhd.premium.utils.mysql;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class PreparedStatementBuilder {

    private int index;
    private PreparedStatement statement;

    public PreparedStatementBuilder(final String sql, MySQL mySQL) {
        statement = mySQL.preparedStatement(sql);
        this.index = 1;
    }

    public final PreparedStatementBuilder bindString(final String bind) {
        try {
            this.statement.setString(this.index, bind);
            this.index++;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public final PreparedStatementBuilder bindInt(final int bind) {
        try {
            this.statement.setInt(this.index, bind);
            this.index++;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public final PreparedStatementBuilder bindBoolean(final boolean bind) {
        try {
            this.statement.setBoolean(this.index, bind);
            this.index++;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public final PreparedStatementBuilder bindLong(final Long bind) {
        try {
            this.statement.setLong(this.index, bind);
            this.index++;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public final PreparedStatementBuilder bindDouble(final Double bind) {
        try {
            this.statement.setDouble(this.index, bind);
            this.index++;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    public final PreparedStatement build() {
        return this.statement;
    }

}
