package de.pattyxdhd.premium.utils.objects;

import de.pattyxdhd.premium.PremiumMain;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.UUID;

@Getter @AllArgsConstructor
public class PremiumPlayer {

    private String uuid;
    private long lastExecute;

    public UUID getUniqueId(){
        return UUID.fromString(this.uuid);
    }

    public void setLastExecute(final Long lastExecute){
        this.lastExecute = lastExecute;
        PremiumMain.getInstance().getDatabaseHandler().updatePlayer(this);
    }

}
