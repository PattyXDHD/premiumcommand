package de.pattyxdhd.premium.listener;

import de.pattyxdhd.premium.PremiumMain;
import de.pattyxdhd.premium.utils.objects.PremiumPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinListener implements Listener {

    @EventHandler
    public void onJoin(final PlayerJoinEvent event){

        if(!PremiumMain.getInstance().getDatabaseHandler().existsPlayer(event.getPlayer().getUniqueId().toString())){
            PremiumMain.getInstance().getDatabaseHandler().insertPlayer(new PremiumPlayer(event.getPlayer().getUniqueId().toString(), 0L));
        }else{
            if(PremiumMain.getInstance().getPlayers().containsKey(event.getPlayer().getUniqueId())) PremiumMain.getInstance().getPlayers().remove(event.getPlayer().getUniqueId());

            PremiumMain.getInstance().getPlayers().put(event.getPlayer().getUniqueId(), PremiumMain.getInstance().getDatabaseHandler().getPlayer(event.getPlayer().getUniqueId().toString()));
        }

    }

}
