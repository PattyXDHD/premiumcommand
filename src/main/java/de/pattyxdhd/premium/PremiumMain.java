package de.pattyxdhd.premium;

import com.google.common.collect.Maps;
import de.pattyxdhd.premium.commands.PremiumCommand;
import de.pattyxdhd.premium.listener.JoinListener;
import de.pattyxdhd.premium.utils.file.FileManager;
import de.pattyxdhd.premium.utils.file.FileWriter;
import de.pattyxdhd.premium.utils.mysql.DatabaseHandler;
import de.pattyxdhd.premium.utils.mysql.MySQL;
import de.pattyxdhd.premium.utils.mysql.PreparedStatementBuilder;
import de.pattyxdhd.premium.utils.objects.PremiumPlayer;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

public class PremiumMain extends JavaPlugin {

    @Getter
    private static PremiumMain instance;

    @Getter
    private Map<UUID, PremiumPlayer> players = Maps.newHashMap();
    @Getter @Setter
    private boolean playersLoaded = false;

    @Getter
    private FileManager fileManager;

    @Getter
    private DatabaseHandler databaseHandler;

    @Override
    public void onEnable() {
        instance = this;
        fileManager = new FileManager(new FileWriter(getDataFolder().getPath(), "config.yml"));

        loadMySQL();

        loadListener(Bukkit.getPluginManager());
        loadCommands();

        log("§aPlugin geladen.");
    }

    @Override
    public void onDisable() {
        getDatabaseHandler().getMySQL().disconnect();
        log("§cPlugin entladen.");
    }

    private void loadCommands(){
        getCommand("premium").setExecutor(new PremiumCommand());
    }

    private void loadListener(final PluginManager pluginManager){
        pluginManager.registerEvents(new JoinListener(), this);
    }

    private void loadMySQL(){
        MySQL mySQL;

        if(getFileManager().isUseSQLite()){
            try {
                Class.forName("org.sqlite.JDBC");
            } catch (ClassNotFoundException e) { e.printStackTrace(); }

            File localFile = new File(getDataFolder().getPath() + File.separator + "data.db");

            if (!localFile.exists()) {
                try {
                    localFile.createNewFile();
                }
                catch (IOException localIOException) {
                    localIOException.printStackTrace();
                }
            }

            mySQL = new MySQL(localFile.getAbsolutePath(), null, null, null, null, getFileManager().getPrefix(), true);
        }else{
            mySQL = new MySQL(getFileManager().getHost(), getFileManager().getPort(), getFileManager().getDatabase(), getFileManager().getUser(), getFileManager().getPassword(), getFileManager().getPrefix(), false);
        }
        mySQL.connect();
        mySQL.update(new PreparedStatementBuilder("CREATE TABLE IF NOT EXISTS PremiumPlayers(uuid VARCHAR(64) PRIMARY KEY, lastExecute BIGINT(50))", mySQL).build());
        databaseHandler = new DatabaseHandler(mySQL);
        new Thread(() -> {
            databaseHandler.loadPlayers();
            Thread.currentThread().stop();
        }).start();
    }

    public void log(final String message){
        Bukkit.getConsoleSender().sendMessage(getFileManager().getPrefix() + message);
    }

}
